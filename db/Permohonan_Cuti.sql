/*
 Navicat Premium Data Transfer

 Source Server         : PGSql
 Source Server Type    : PostgreSQL
 Source Server Version : 120002
 Source Host           : localhost:5432
 Source Catalog        : Permohonan_Cuti
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120002
 File Encoding         : 65001

 Date: 09/04/2020 11:55:10
*/


-- ----------------------------
-- Sequence structure for bucket_approval_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bucket_approval_seq";
CREATE SEQUENCE "public"."bucket_approval_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for position_leave_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."position_leave_seq";
CREATE SEQUENCE "public"."position_leave_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for positions_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."positions_seq";
CREATE SEQUENCE "public"."positions_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_leave_request_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_leave_request_seq";
CREATE SEQUENCE "public"."user_leave_request_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_seq";
CREATE SEQUENCE "public"."users_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Table structure for bucket_approval
-- ----------------------------
DROP TABLE IF EXISTS "public"."bucket_approval";
CREATE TABLE "public"."bucket_approval" (
  "id_bucket_approval" int4 NOT NULL,
  "id_user" int4,
  "resolver_reason" text COLLATE "pg_catalog"."default",
  "resolved_by" varchar(30) COLLATE "pg_catalog"."default",
  "resolved_date" date,
  "status" varchar(15) COLLATE "pg_catalog"."default",
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" timestamp(6)
)
;

-- ----------------------------
-- Records of bucket_approval
-- ----------------------------
INSERT INTO "public"."bucket_approval" VALUES (1, NULL, NULL, NULL, NULL, 'waiting', 'Reza', '2020-04-09 10:54:15.63', 'Reza', '2020-04-09 10:54:15.644');
INSERT INTO "public"."bucket_approval" VALUES (2, NULL, NULL, NULL, NULL, 'waiting', 'Reza', '2020-04-09 10:54:44.348', 'Reza', '2020-04-09 10:54:44.362');
INSERT INTO "public"."bucket_approval" VALUES (3, NULL, NULL, NULL, NULL, 'waiting', 'Reza', '2020-04-09 10:55:09.48', 'Reza', '2020-04-09 10:55:09.499');
INSERT INTO "public"."bucket_approval" VALUES (4, 1, 'Dipersilahkan', 'Indra', '2020-06-10', NULL, 'Reza', '2020-04-09 11:30:09.618', 'Reza', '2020-04-09 11:30:09.618');
INSERT INTO "public"."bucket_approval" VALUES (5, 1, 'Dipersilahkan', 'Indra', '2020-06-10', NULL, 'Reza', '2020-04-09 11:31:45.942', 'Reza', '2020-04-09 11:31:45.942');
INSERT INTO "public"."bucket_approval" VALUES (6, 1, 'Dipersilahkan', 'Indra', '2020-06-10', NULL, 'Reza', '2020-04-09 11:31:48.772', 'Reza', '2020-04-09 11:31:48.772');
INSERT INTO "public"."bucket_approval" VALUES (7, 1, 'Dipersilahkan', 'Indra', '2020-06-10', 'approved', 'Reza', '2020-04-09 11:32:10.251', 'Reza', '2020-04-09 11:32:10.251');
INSERT INTO "public"."bucket_approval" VALUES (8, 1, 'Dipersilahkan', 'Indra', '2020-06-10', 'approved', 'Reza', '2020-04-09 11:35:14.039', 'Reza', '2020-04-09 11:35:14.039');
INSERT INTO "public"."bucket_approval" VALUES (9, 1, 'Dipersilahkan', 'Indra', '2020-06-10', 'approved', 'Reza', '2020-04-09 11:35:14.618', 'Reza', '2020-04-09 11:35:14.618');
INSERT INTO "public"."bucket_approval" VALUES (10, 1, 'Dipersilahkan', 'Indra', '2020-06-10', 'approved', 'Reza', '2020-04-09 11:35:15.683', 'Reza', '2020-04-09 11:35:15.683');
INSERT INTO "public"."bucket_approval" VALUES (11, 1, 'Dipersilahkan', 'Indra', '2020-06-10', 'approved', 'Reza', '2020-04-09 11:53:19.701', 'Reza', '2020-04-09 11:53:19.701');

-- ----------------------------
-- Table structure for position_leave
-- ----------------------------
DROP TABLE IF EXISTS "public"."position_leave";
CREATE TABLE "public"."position_leave" (
  "id_position_leave" int4 NOT NULL,
  "id_position" int4,
  "total_leave" int4 NOT NULL,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" timestamp(6)
)
;

-- ----------------------------
-- Records of position_leave
-- ----------------------------
INSERT INTO "public"."position_leave" VALUES (1, 1, 12, 'Reza', '2020-04-08 23:21:47.354', 'Reza', '2020-04-08 23:21:47.354');
INSERT INTO "public"."position_leave" VALUES (2, 2, 15, 'Reza', '2020-04-08 23:21:51.528', 'Reza', '2020-04-08 23:21:51.528');
INSERT INTO "public"."position_leave" VALUES (3, 3, 13, 'Reza', '2020-04-08 23:21:57.231', 'Reza', '2020-04-08 23:21:57.231');

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS "public"."positions";
CREATE TABLE "public"."positions" (
  "id_position" int4 NOT NULL,
  "position_name" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" timestamp(6)
)
;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO "public"."positions" VALUES (1, 'Employee', 'Reza', '2020-04-08 23:14:42.034', 'Reza', '2020-04-08 23:14:42.034');
INSERT INTO "public"."positions" VALUES (2, 'Supervisor', 'Reza', '2020-04-08 23:14:48.981', 'Reza', '2020-04-08 23:14:48.981');
INSERT INTO "public"."positions" VALUES (3, 'Staff', 'Reza', '2020-04-08 23:14:54.53', 'Reza', '2020-04-08 23:14:54.53');

-- ----------------------------
-- Table structure for user_leave_request
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_leave_request";
CREATE TABLE "public"."user_leave_request" (
  "id_user_leave" int4 NOT NULL,
  "id_user" int4,
  "id_bucket_approval" int4,
  "leave_date_from" date NOT NULL,
  "leave_date_to" date NOT NULL,
  "description" text COLLATE "pg_catalog"."default" NOT NULL,
  "sisa_cuti" int4,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" timestamp(6)
)
;

-- ----------------------------
-- Records of user_leave_request
-- ----------------------------
INSERT INTO "public"."user_leave_request" VALUES (1, 1, 1, '2020-04-30', '2020-05-01', 'Lagi males', 12, 'Reza', '2020-04-09 10:54:15.613', 'Reza', '2020-04-09 10:54:15.641');
INSERT INTO "public"."user_leave_request" VALUES (2, 2, 2, '2020-04-30', '2020-05-01', 'Istirahat panjang', 15, 'Reza', '2020-04-09 10:54:44.324', 'Reza', '2020-04-09 10:54:44.358');
INSERT INTO "public"."user_leave_request" VALUES (3, 3, 3, '2020-04-10', '2020-04-11', 'ILagi cape', 13, 'Reza', '2020-04-09 10:55:09.462', 'Reza', '2020-04-09 10:55:09.496');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id_user" int4 NOT NULL,
  "id_position" int4,
  "user_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "user_gender" varchar(10) COLLATE "pg_catalog"."default" NOT NULL,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" timestamp(6)
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 1, 'Septian Reza', 'Male', 'Reza', '2020-04-08 23:15:17.925', 'Reza', '2020-04-08 23:15:17.925');
INSERT INTO "public"."users" VALUES (2, 2, 'Desy Indriani', 'Female', 'Reza', '2020-04-08 23:15:33.647', 'Reza', '2020-04-08 23:15:33.647');
INSERT INTO "public"."users" VALUES (3, 3, 'Cecep', 'Male', 'Reza', '2020-04-08 23:15:46.77', 'Reza', '2020-04-08 23:15:46.77');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."bucket_approval_seq"', 12, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."position_leave_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."positions_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."user_leave_request_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."users_seq"', 4, true);

-- ----------------------------
-- Indexes structure for table bucket_approval
-- ----------------------------
CREATE UNIQUE INDEX "bucket_approval_pk" ON "public"."bucket_approval" USING btree (
  "id_bucket_approval" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "bucket_to_user_fk" ON "public"."bucket_approval" USING btree (
  "id_user" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table bucket_approval
-- ----------------------------
ALTER TABLE "public"."bucket_approval" ADD CONSTRAINT "pk_bucket_approval" PRIMARY KEY ("id_bucket_approval");

-- ----------------------------
-- Indexes structure for table position_leave
-- ----------------------------
CREATE UNIQUE INDEX "position_leave_pk" ON "public"."position_leave" USING btree (
  "id_position_leave" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "position_to_pos_leave_fk" ON "public"."position_leave" USING btree (
  "id_position" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table position_leave
-- ----------------------------
ALTER TABLE "public"."position_leave" ADD CONSTRAINT "pk_position_leave" PRIMARY KEY ("id_position_leave");

-- ----------------------------
-- Indexes structure for table positions
-- ----------------------------
CREATE UNIQUE INDEX "positions_pk" ON "public"."positions" USING btree (
  "id_position" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table positions
-- ----------------------------
ALTER TABLE "public"."positions" ADD CONSTRAINT "pk_positions" PRIMARY KEY ("id_position");

-- ----------------------------
-- Indexes structure for table user_leave_request
-- ----------------------------
CREATE INDEX "bucket_to_user_leave_req_fk" ON "public"."user_leave_request" USING btree (
  "id_bucket_approval" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "user_leave_request_pk" ON "public"."user_leave_request" USING btree (
  "id_user_leave" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "user_to_user_leave_req_fk" ON "public"."user_leave_request" USING btree (
  "id_user" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user_leave_request
-- ----------------------------
ALTER TABLE "public"."user_leave_request" ADD CONSTRAINT "pk_user_leave_request" PRIMARY KEY ("id_user_leave");

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE INDEX "user_to_position_fk" ON "public"."users" USING btree (
  "id_position" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "users_pk" ON "public"."users" USING btree (
  "id_user" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "pk_users" PRIMARY KEY ("id_user");

-- ----------------------------
-- Foreign Keys structure for table bucket_approval
-- ----------------------------
ALTER TABLE "public"."bucket_approval" ADD CONSTRAINT "fk_bucket_a_bucket_to_users" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id_user") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table position_leave
-- ----------------------------
ALTER TABLE "public"."position_leave" ADD CONSTRAINT "fk_position_position__position" FOREIGN KEY ("id_position") REFERENCES "public"."positions" ("id_position") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table user_leave_request
-- ----------------------------
ALTER TABLE "public"."user_leave_request" ADD CONSTRAINT "fk_user_lea_bucket_to_bucket_a" FOREIGN KEY ("id_bucket_approval") REFERENCES "public"."bucket_approval" ("id_bucket_approval") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."user_leave_request" ADD CONSTRAINT "fk_user_lea_user_to_u_users" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id_user") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "fk_users_user_to_p_position" FOREIGN KEY ("id_position") REFERENCES "public"."positions" ("id_position") ON DELETE RESTRICT ON UPDATE RESTRICT;
