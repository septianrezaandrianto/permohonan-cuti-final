/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     08/04/2020 22:18:04                          */
/*==============================================================*/


drop index BUCKET_TO_USER_FK;

drop index BUCKET_APPROVAL_PK;

drop table BUCKET_APPROVAL;

drop index POSITIONS_PK;

drop table POSITIONS;

drop index POSITION_TO_POS_LEAVE_FK;

drop index POSITION_LEAVE_PK;

drop table POSITION_LEAVE;

drop index USER_TO_POSITION_FK;

drop index USERS_PK;

drop table USERS;

drop index BUCKET_TO_USER_LEAVE_REQ_FK;

drop index USER_TO_USER_LEAVE_REQ_FK;

drop index USER_LEAVE_REQUEST_PK;

drop table USER_LEAVE_REQUEST;

/*==============================================================*/
/* Table: BUCKET_APPROVAL                                       */
/*==============================================================*/
create table BUCKET_APPROVAL (
   ID_BUCKET_APPROVAL   INT4                 not null,
   ID_USER              INT4                 null,
   RESOLVER_REASON      TEXT                 not null,
   RESOLVED_BY          VARCHAR(30)          not null,
   RESOLVED_DATE        DATE                 not null,
   STATUS               VARCHAR(15)          null,
   constraint PK_BUCKET_APPROVAL primary key (ID_BUCKET_APPROVAL)
);

/*==============================================================*/
/* Index: BUCKET_APPROVAL_PK                                    */
/*==============================================================*/
create unique index BUCKET_APPROVAL_PK on BUCKET_APPROVAL (
ID_BUCKET_APPROVAL
);

/*==============================================================*/
/* Index: BUCKET_TO_USER_FK                                     */
/*==============================================================*/
create  index BUCKET_TO_USER_FK on BUCKET_APPROVAL (
ID_USER
);

/*==============================================================*/
/* Table: POSITIONS                                             */
/*==============================================================*/
create table POSITIONS (
   ID_POSITION          INT4                 not null,
   POSITION_NAME        VARCHAR(20)          not null,
   constraint PK_POSITIONS primary key (ID_POSITION)
);

/*==============================================================*/
/* Index: POSITIONS_PK                                          */
/*==============================================================*/
create unique index POSITIONS_PK on POSITIONS (
ID_POSITION
);

/*==============================================================*/
/* Table: POSITION_LEAVE                                        */
/*==============================================================*/
create table POSITION_LEAVE (
   ID_POSITION_LEAVE    INT4                 not null,
   ID_POSITION          INT4                 null,
   TOTAL_LEAVE          INT4                 not null,
   constraint PK_POSITION_LEAVE primary key (ID_POSITION_LEAVE)
);

/*==============================================================*/
/* Index: POSITION_LEAVE_PK                                     */
/*==============================================================*/
create unique index POSITION_LEAVE_PK on POSITION_LEAVE (
ID_POSITION_LEAVE
);

/*==============================================================*/
/* Index: POSITION_TO_POS_LEAVE_FK                              */
/*==============================================================*/
create  index POSITION_TO_POS_LEAVE_FK on POSITION_LEAVE (
ID_POSITION
);

/*==============================================================*/
/* Table: USERS                                                 */
/*==============================================================*/
create table USERS (
   ID_USER              INT4                 not null,
   ID_POSITION          INT4                 null,
   USER_NAME            VARCHAR(30)          not null,
   USER_GENDER          VARCHAR(10)          not null,
   constraint PK_USERS primary key (ID_USER)
);

/*==============================================================*/
/* Index: USERS_PK                                              */
/*==============================================================*/
create unique index USERS_PK on USERS (
ID_USER
);

/*==============================================================*/
/* Index: USER_TO_POSITION_FK                                   */
/*==============================================================*/
create  index USER_TO_POSITION_FK on USERS (
ID_POSITION
);

/*==============================================================*/
/* Table: USER_LEAVE_REQUEST                                    */
/*==============================================================*/
create table USER_LEAVE_REQUEST (
   ID_USER_LEAVE        INT4                 not null,
   ID_USER              INT4                 null,
   ID_BUCKET_APPROVAL   INT4                 null,
   LEAVE_DATE_FROM      DATE                 not null,
   LEAVE_DATE_TO        DATE                 not null,
   DESCRIPTION          TEXT                 not null,
   SISA_CUTI            INT4                 null,
   constraint PK_USER_LEAVE_REQUEST primary key (ID_USER_LEAVE)
);

/*==============================================================*/
/* Index: USER_LEAVE_REQUEST_PK                                 */
/*==============================================================*/
create unique index USER_LEAVE_REQUEST_PK on USER_LEAVE_REQUEST (
ID_USER_LEAVE
);

/*==============================================================*/
/* Index: USER_TO_USER_LEAVE_REQ_FK                             */
/*==============================================================*/
create  index USER_TO_USER_LEAVE_REQ_FK on USER_LEAVE_REQUEST (
ID_USER
);

/*==============================================================*/
/* Index: BUCKET_TO_USER_LEAVE_REQ_FK                           */
/*==============================================================*/
create  index BUCKET_TO_USER_LEAVE_REQ_FK on USER_LEAVE_REQUEST (
ID_BUCKET_APPROVAL
);

alter table BUCKET_APPROVAL
   add constraint FK_BUCKET_A_BUCKET_TO_USERS foreign key (ID_USER)
      references USERS (ID_USER)
      on delete restrict on update restrict;

alter table POSITION_LEAVE
   add constraint FK_POSITION_POSITION__POSITION foreign key (ID_POSITION)
      references POSITIONS (ID_POSITION)
      on delete restrict on update restrict;

alter table USERS
   add constraint FK_USERS_USER_TO_P_POSITION foreign key (ID_POSITION)
      references POSITIONS (ID_POSITION)
      on delete restrict on update restrict;

alter table USER_LEAVE_REQUEST
   add constraint FK_USER_LEA_BUCKET_TO_BUCKET_A foreign key (ID_BUCKET_APPROVAL)
      references BUCKET_APPROVAL (ID_BUCKET_APPROVAL)
      on delete restrict on update restrict;

alter table USER_LEAVE_REQUEST
   add constraint FK_USER_LEA_USER_TO_U_USERS foreign key (ID_USER)
      references USERS (ID_USER)
      on delete restrict on update restrict;

