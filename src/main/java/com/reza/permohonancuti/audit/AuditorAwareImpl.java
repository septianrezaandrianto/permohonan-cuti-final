package com.reza.permohonancuti.audit;

import java.util.Optional;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.reza.permohonancuti.model.Users;

import ch.qos.logback.classic.Logger;

public class AuditorAwareImpl implements AuditorAware<String> {

	Users users = new Users();
	
	@Override
	public Optional<String> getCurrentAuditor() {
		
		return Optional.of("Reza");
	}
}
