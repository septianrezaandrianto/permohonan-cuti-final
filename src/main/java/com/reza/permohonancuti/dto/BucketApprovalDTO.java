package com.reza.permohonancuti.dto;

import java.util.Date;
public class BucketApprovalDTO {

	 private int idBucketApproval;
     private UsersDTO usersDto;
     private String resolverReason;
     private String resolvedBy;
     private Date resolvedDate;
     private String status;
	
     public BucketApprovalDTO() {
		super();
     }

	public BucketApprovalDTO(int idBucketApproval, UsersDTO usersDto, String resolverReason, String resolvedBy,
			Date resolvedDate, String status) {
		super();
		this.idBucketApproval = idBucketApproval;
		this.usersDto = usersDto;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getIdBucketApproval() {
		return idBucketApproval;
	}

	public void setIdBucketApproval(int idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	public UsersDTO getUsersDto() {
		return usersDto;
	}


	public void setUsersDto(UsersDTO usersDto) {
		this.usersDto = usersDto;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	
}
