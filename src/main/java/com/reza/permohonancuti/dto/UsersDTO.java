package com.reza.permohonancuti.dto;

public class UsersDTO {

	 private int idUser;
     private PositionsDTO positionsDto;
     private String userName;
     private String userGender;     
     
	public UsersDTO() {
		super();
	}

	public UsersDTO(int idUser, PositionsDTO positionsDto, String userName, String userGender) {
		super();
		this.idUser = idUser;
		this.positionsDto = positionsDto;
		this.userName = userName;
		this.userGender = userGender;
	}


	public int getIdUser() {
		return idUser;
	}


	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}


	public PositionsDTO getPositionsDto() {
		return positionsDto;
	}

	public void setPositionsDto(PositionsDTO positionsDto) {
		this.positionsDto = positionsDto;
	}

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserGender() {
		return userGender;
	}


	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}


}
