package com.reza.permohonancuti.dto;

public class PositionsDTO {

	 private int idPosition;
     private String positionName;
     
     
	public PositionsDTO() {
		super();
	}

	public PositionsDTO(int idPosition, String positionName) {
		super();
		this.idPosition = idPosition;
		this.positionName = positionName;
	}

	public int getIdPosition() {
		return idPosition;
	}

	public void setIdPosition(int idPosition) {
		this.idPosition = idPosition;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}


	
}
