package com.reza.permohonancuti.dto;

import java.util.Date;

public class UserLeaveRequestDTO {

	 private int idUserLeave;
     private UsersDTO usersDto;
     private BucketApprovalDTO bucketApprovalDto;
     private Date leaveDateFrom;
     private Date leaveDateTo;
     private String description;
     private int sisaCuti;
     
	public UserLeaveRequestDTO() {
		super();
	}


	public UserLeaveRequestDTO(int idUserLeave, UsersDTO usersDto, BucketApprovalDTO bucketApprovalDto,
			Date leaveDateFrom, Date leaveDateTo, String description, int sisaCuti) {
		super();
		this.idUserLeave = idUserLeave;
		this.usersDto = usersDto;
		this.bucketApprovalDto = bucketApprovalDto;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.sisaCuti = sisaCuti;
	}



	public int getIdUserLeave() {
		return idUserLeave;
	}

	public void setIdUserLeave(int idUserLeave) {
		this.idUserLeave = idUserLeave;
	}

	public UsersDTO getUsersDto() {
		return usersDto;
	}

	public void setUsersDto(UsersDTO usersDto) {
		this.usersDto = usersDto;
	}

	public BucketApprovalDTO getBucketApprovalDto() {
		return bucketApprovalDto;
	}

	public void setBucketApprovalDto(BucketApprovalDTO bucketApprovalDto) {
		this.bucketApprovalDto = bucketApprovalDto;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public int getSisaCuti() {
		return sisaCuti;
	}


	public void setSisaCuti(int sisaCuti) {
		this.sisaCuti = sisaCuti;
	}

	

}
