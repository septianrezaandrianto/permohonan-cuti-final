package com.reza.permohonancuti.dto;

public class PositionLeaveDTO {

	 private int idPositionLeave;
     private PositionsDTO positionsDto;
     private int totalLeave;
         
     public PositionLeaveDTO() {
		super();
     }

	public PositionLeaveDTO(int idPositionLeave, PositionsDTO positionsDto, int totalLeave) {
		super();
		this.idPositionLeave = idPositionLeave;
		this.positionsDto = positionsDto;
		this.totalLeave = totalLeave;
	}


	public int getIdPositionLeave() {
		return idPositionLeave;
	}

	public void setIdPositionLeave(int idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	public PositionsDTO getPositionsDto() {
		return positionsDto;
	}

	public void setPositionsDto(PositionsDTO positionsDto) {
		this.positionsDto = positionsDto;
	}

	public int getTotalLeave() {
		return totalLeave;
	}

	public void setTotalLeave(int totalLeave) {
		this.totalLeave = totalLeave;
	}

	

}
