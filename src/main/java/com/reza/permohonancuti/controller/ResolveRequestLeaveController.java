package com.reza.permohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reza.permohonancuti.dto.BucketApprovalDTO;
import com.reza.permohonancuti.model.BucketApproval;
import com.reza.permohonancuti.model.UserLeaveRequest;
import com.reza.permohonancuti.repository.BucketApprovalRepository;
import com.reza.permohonancuti.repository.UserLeaveRequestRepository;

@RestController
@RequestMapping("/api")
public class ResolveRequestLeaveController {

	@Autowired
	private BucketApprovalRepository bucketApprovalRepository;
	@Autowired
	private UserLeaveRequestRepository userLeaveRequestRepository;
	
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity to DTO
	public BucketApprovalDTO convertToDTO(BucketApproval bucketApproval) {
		BucketApprovalDTO baDto = modelMapper.map(bucketApproval, BucketApprovalDTO.class);
	return baDto;
	}
	
	public BucketApproval convertToEntity (BucketApprovalDTO bucketApprovalDTO) {
		BucketApproval ba = modelMapper.map(bucketApprovalDTO, BucketApproval.class);
	return ba;
	}
	
	
	@PostMapping("/resolverequest/add")
	public HashMap<String, Object> addBucketApproval(@Valid @RequestBody BucketApprovalDTO bucketApprovalDTO) {
		
		String message="";
		
		HashMap<String, Object> hasMapBucketApproval = new HashMap<String, Object>();		
						
			BucketApproval approve = bucketApprovalRepository.findById(bucketApprovalDTO.getIdBucketApproval()).orElse(null);			
						
				if(approve == null) {
					message = "Permohonan anda tidak ditemukan";
				}else {	

					for (BucketApproval ba : bucketApprovalRepository.findAll()) {
						
						if (ba.getIdBucketApproval() == bucketApprovalDTO.getIdBucketApproval()) {
							
							ba.setResolvedBy(bucketApprovalDTO.getResolvedBy());
							ba.setResolvedDate(bucketApprovalDTO.getResolvedDate());
							ba.setResolverReason(bucketApprovalDTO.getResolverReason());
							bucketApprovalRepository.save(ba);
							//approve = ba;
						}
					}	
					
					//approve.setStatus(bucketApprovalDTO.getStatus());
					
//					BucketApprovalDTO baDto = convertToDTO(approve);
						
					hasMapBucketApproval.put("test", "Show All Data");
					hasMapBucketApproval.put("Data", approve);
				}
					
				
			
				hasMapBucketApproval.put("messages", message);
				hasMapBucketApproval.put("idnya", bucketApprovalDTO.getIdBucketApproval());
		
	return hasMapBucketApproval;
	}
	
}
