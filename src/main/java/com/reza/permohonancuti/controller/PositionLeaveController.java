package com.reza.permohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reza.permohonancuti.dto.PositionLeaveDTO;
import com.reza.permohonancuti.dto.PositionsDTO;
import com.reza.permohonancuti.model.PositionLeave;
import com.reza.permohonancuti.model.Positions;
import com.reza.permohonancuti.repository.PositionLeaveRepository;

@RestController
@RequestMapping("/api")
public class PositionLeaveController {

	@Autowired
	private PositionLeaveRepository positionLeaveRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity To DTO
	public PositionLeaveDTO convertPositionLeaveToDTO(PositionLeave positionLeave) {
		PositionLeaveDTO plDto = modelMapper.map(positionLeave, PositionLeaveDTO.class);
		
		if (positionLeave.getPositions() != null) {
			PositionsDTO psDto = modelMapper.map(positionLeave.getPositions(), PositionsDTO.class);
			plDto.setPositionsDto(psDto);
		}

	return plDto;
	}
	
//	Convert DTO To Entity
	public PositionLeave convertPositionLeaveToEntity(PositionLeaveDTO positionLeaveDTO) {
		PositionLeave pl = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		
		if (positionLeaveDTO.getPositionsDto() != null) {
			Positions pos = modelMapper.map(positionLeaveDTO.getPositionsDto(), Positions.class);
			pl.setPositions(pos);
		}
	return pl;
	}
	
//	Melihat Position Leave
	@GetMapping("/positionleave/all")
	public HashMap<String, Object> getAllPositionLeave() {
		
		String message;
		
		HashMap<String, Object> hasMapPosLeave = new HashMap<String, Object>();
		ArrayList<PositionLeaveDTO> posLeaveDtoList = new ArrayList<PositionLeaveDTO>();
		
		for (PositionLeave pl : positionLeaveRepository.findAll()) {
			PositionLeaveDTO plDto = convertPositionLeaveToDTO(pl);
			posLeaveDtoList.add(plDto);
		}
		
		if (posLeaveDtoList.isEmpty()) {
			message = "Maaf, data masih kosong";
			hasMapPosLeave.put("Message", message);
		}
		else {
			message = "Show All Data";
			hasMapPosLeave.put("Message", message);
			hasMapPosLeave.put("Total" , posLeaveDtoList.size());
			hasMapPosLeave.put("Data" , posLeaveDtoList);
		}
		
		
	return hasMapPosLeave;
	}
	
//	Melihat position leave by Id
	@GetMapping("/positionleave/{id}")
	public HashMap<String, Object> getPositionLeaveById(@PathVariable(value = "id") Integer idPositionLeave) {
		
		HashMap<String, Object> hasMapPosLeave = new HashMap<String, Object>();
		
		PositionLeave pl = positionLeaveRepository.findById(idPositionLeave).orElse(null);
		
		PositionLeaveDTO plDto = convertPositionLeaveToDTO(pl);
		
		hasMapPosLeave.put("Message" , "Data By Id");
		hasMapPosLeave.put("Data" , plDto);
	return hasMapPosLeave;
	} 
	
//	Delete Position
	@DeleteMapping("/positionleave/delete/{id}")
	public HashMap<String, Object> deletePositionLeave(@PathVariable(value = "id") Integer idPositionLeave) {
		
		HashMap<String, Object> hasMapPosLeave = new HashMap<String, Object>();
		
		PositionLeave pl = positionLeaveRepository.findById(idPositionLeave).orElse(null);
		positionLeaveRepository.delete(pl);
		
		hasMapPosLeave.put("Message" , "Delete Success");
	return hasMapPosLeave;
	}
	
//	Menambah Position Leave
	@PostMapping("/positionleave/add")
	public HashMap<String, Object> addPositionLeave (@Valid @RequestBody PositionLeaveDTO positionLeaveDTO) {
		
		HashMap<String, Object> hasMapPosLeave = new HashMap<String, Object>();
		
		PositionLeave pl = convertPositionLeaveToEntity(positionLeaveDTO);
		
		hasMapPosLeave.put("Message" , "Add Success");
		hasMapPosLeave.put("Data" , positionLeaveRepository.save(pl));
	return hasMapPosLeave;
				
	}
	
//	Mengupdate Position Leave
	@PutMapping("/positionleave/update/{id}")
	public HashMap<String, Object> updatePositionLeave(@PathVariable(value = "id") Integer PositionLeave, @Valid @RequestBody PositionLeaveDTO positionLeaveDTO) {
		
		HashMap<String, Object> hasMapPosLeave = new HashMap<String, Object>();
		
		PositionLeave pl = positionLeaveRepository.findById(PositionLeave).orElse(null);
		
		positionLeaveDTO.setIdPositionLeave(pl.getIdPositionLeave());
		
		if (positionLeaveDTO.getPositionsDto() != null) {
			pl.setPositions(convertPositionLeaveToEntity(positionLeaveDTO).getPositions());
		}
		if (positionLeaveDTO.getTotalLeave() != 0) {
			pl.setTotalLeave(convertPositionLeaveToEntity(positionLeaveDTO).getTotalLeave());
		}
		positionLeaveRepository.save(pl);
		PositionLeaveDTO plDto = convertPositionLeaveToDTO(pl);
		
		hasMapPosLeave.put("Message" , "Update Success");
		hasMapPosLeave.put("Data" , plDto);
		
	return hasMapPosLeave;
	}
	
}
