package com.reza.permohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.crypto.spec.PSource;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reza.permohonancuti.dto.PositionsDTO;
import com.reza.permohonancuti.model.Positions;
import com.reza.permohonancuti.repository.PositionsRepository;

@RestController
@RequestMapping("/api")
public class PositionsController {

	@Autowired
	private PositionsRepository positionsRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity To DTO
	public PositionsDTO convertPositionsToDTO (Positions positions) {
		
		PositionsDTO positionDto = modelMapper.map(positions, PositionsDTO.class);
	return positionDto;
	}
	
//	Convert DTO To Entity
	public Positions convertPositionsToEntity (PositionsDTO positionsDTO) {
		
		Positions positions = modelMapper.map(positionsDTO, Positions.class);		
	return positions;
	}
	
//	Melihat data Positions
	@GetMapping("/positions/all")
	public HashMap<String, Object> getAllPositions() {
		
		HashMap<String, Object> hasMapPositions = new HashMap<String, Object>();
		
		ArrayList<PositionsDTO> positionsDtoList = new ArrayList<PositionsDTO>();
		
		for (Positions pos : positionsRepository.findAll()) {
			PositionsDTO positionDto = convertPositionsToDTO(pos);
			positionsDtoList.add(positionDto);
		}
		hasMapPositions.put("Message", "Show All Data");
		hasMapPositions.put("Total", positionsDtoList.size());
		hasMapPositions.put("Data", positionsDtoList);
		
	return hasMapPositions;
	}
	
//	Melihat positions by id
	@GetMapping("/positions/{id}")
	public HashMap<String, Object> getPositionsById (@PathVariable(value = "id") Integer idPosition) {
		
		HashMap<String, Object> hasMapPosition = new HashMap<String, Object>();
		
		Positions positions = positionsRepository.findById(idPosition).orElse(null);
		
		PositionsDTO positionsDto = convertPositionsToDTO(positions);
		
		hasMapPosition.put("Message", "Data By Id");
		hasMapPosition.put("Data", positionsDto);
		
	return hasMapPosition;
	}
	
//	Mengapus positions
	@DeleteMapping("/positions/delete/{id}")
	public HashMap<String, Object> deletePositions (@PathVariable(value = "id") Integer idPosition) {
		
		HashMap<String, Object> hasMapPositions = new HashMap<String, Object>();
		
		Positions positions = positionsRepository.findById(idPosition).orElse(null);
		positionsRepository.delete(positions);
		
		hasMapPositions.put("Message" , "Delete Success");
		
	return hasMapPositions;
	}
	
//	Menambahkan Positions
	@PostMapping("/positions/add")
	public HashMap<String, Object> addPositions (@Valid @RequestBody PositionsDTO positionsDTO) {
		
		HashMap<String, Object> hasMapPositions = new HashMap<String, Object>();
		
		Positions positions = convertPositionsToEntity(positionsDTO);
		
		hasMapPositions.put("Message" , "Add Success");
		hasMapPositions.put("Data", positionsRepository.save(positions));
		
	return hasMapPositions;
	}
	
//	Mengupdate Positions
	@PutMapping("/positions/update/{id}")
	public HashMap<String, Object> updatePositions (@PathVariable(value = "id") Integer idPosition, @Valid @RequestBody PositionsDTO positionsDTO) {
		
		HashMap<String, Object> hasMapPositions = new HashMap<String, Object>();
		
		Positions positions = positionsRepository.findById(idPosition).orElse(null);
		
		positionsDTO.setIdPosition(positions.getIdPosition());
		
		positions.setPositionName(convertPositionsToEntity(positionsDTO).getPositionName());
		positionsRepository.save(positions);
		PositionsDTO posDto = convertPositionsToDTO(positions);
		
		hasMapPositions.put("Message", "Update Success");
		hasMapPositions.put("Data", posDto );
	
	return hasMapPositions; 
	}
	
} 
