package com.reza.permohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reza.permohonancuti.dto.PositionsDTO;
import com.reza.permohonancuti.dto.UsersDTO;
import com.reza.permohonancuti.model.Positions;
import com.reza.permohonancuti.model.Users;
import com.reza.permohonancuti.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class UsersController {

	@Autowired
	private UsersRepository usersRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	
//	Convert Entity To DTO
	public UsersDTO convertUSersToDTO(Users users) {
		UsersDTO usersDto = modelMapper.map(users, UsersDTO.class);
		
		if (users.getPositions() != null) {
			PositionsDTO posDto = modelMapper.map(users.getPositions(), PositionsDTO.class);
			usersDto.setPositionsDto(posDto);
		}
	return usersDto; 
	}
	
//	Convert DTO To Entity
	public Users convertUserToEntity (UsersDTO usersDTO) {
		Users users = modelMapper.map(usersDTO, Users.class);

		if (usersDTO.getPositionsDto() != null) {
			Positions pos = modelMapper.map(usersDTO.getPositionsDto(), Positions.class);
			users.setPositions(pos);
		}
	return users;
	}
	
	
//	Melihat data users
	@GetMapping("/users/all")
	public HashMap<String, Object> getAllUsers() {
		
		String message = "";
		HashMap<String, Object> hasMapUsers = new HashMap<String, Object>();
		ArrayList<UsersDTO> usersDtoList = new ArrayList<UsersDTO>();
		
		for (Users users : usersRepository.findAll()) {
			
			UsersDTO usersDto = convertUSersToDTO(users);
			usersDtoList.add(usersDto);
		}
		
		if (usersDtoList.isEmpty()) {
			message = "Maaf, data masih kosong";
			hasMapUsers.put("Message", message);
		}
		else {
			message = "Show All Data";
			hasMapUsers.put("Message", message);
			hasMapUsers.put("Total" , usersDtoList.size());
			hasMapUsers.put("Data", usersDtoList);
		}
		
	return hasMapUsers;	
	}
	
//	Melihat data users by Id
	@GetMapping("/users/{id}")
	public HashMap<String, Object> getUsersById (@PathVariable(value = "id") Integer IdUser) {
		
		HashMap<String, Object> hasMapUsers = new HashMap<String, Object>();
		
		Users users = usersRepository.findById(IdUser).orElse(null);
		
		UsersDTO usersDto = convertUSersToDTO(users);
		
		hasMapUsers.put("Message", "Data By Id");
		hasMapUsers.put("Data", usersDto);
	
	return hasMapUsers;
	}
	
//	Mendelete Users
	@DeleteMapping("/users/delete/{id}")
	public HashMap<String, Object> deleteUsers(@PathVariable(value = "id") Integer idUser) {
		
		HashMap<String, Object> hasMapUsers = new HashMap<String, Object>();
		
		Users users = usersRepository.findById(idUser).orElse(null);
		
		usersRepository.delete(users);
		
		hasMapUsers.put("Message", "Delete Success");
		
	return hasMapUsers;
	}
	
//	Update Users
	@PutMapping("/users/update/{id}")
	public HashMap<String, Object> updateUsers(@PathVariable(value = "id") Integer idUser, @Valid @RequestBody UsersDTO usersDTO) {
		
		HashMap<String, Object> hasMapUsers = new HashMap<String, Object>();
		
		Users users = usersRepository.findById(idUser).orElse(null);
		
		usersDTO.setIdUser(users.getIdUser());
		
		if (usersDTO.getPositionsDto() != null) {
			users.setPositions(convertUserToEntity(usersDTO).getPositions());
		}
		if (usersDTO.getUserName() != null) {
			users.setUserName(convertUserToEntity(usersDTO).getUserName());
		}
		if (usersDTO.getUserGender() != null) {
			users.setUserGender(convertUserToEntity(usersDTO).getUserGender());
		}
		
		usersRepository.save(users);
		UsersDTO usersDto = convertUSersToDTO(users);
		
		hasMapUsers.put("Message", "Update Success");
		hasMapUsers.put("Data", usersDto);
	return hasMapUsers;
	}
	
//	Menambahkan users
	@PostMapping("/users/add")
	public HashMap<String, Object> addUsers(@Valid @RequestBody UsersDTO usersDTO) {
		
		HashMap<String, Object> hasMapUsers = new HashMap<String, Object>();
		
		Users users = convertUserToEntity(usersDTO);

		
		hasMapUsers.put("Message", "Add Success");
		hasMapUsers.put("Data" , usersRepository.save(users));
		
	return hasMapUsers;
	}
	
}
