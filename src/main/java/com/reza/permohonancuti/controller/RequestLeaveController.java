package com.reza.permohonancuti.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.domain.Pageable;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.reza.permohonancuti.dto.UserLeaveRequestDTO;
import com.reza.permohonancuti.dto.UsersDTO;
import com.reza.permohonancuti.model.BucketApproval;
import com.reza.permohonancuti.model.PositionLeave;
import com.reza.permohonancuti.model.UserLeaveRequest;
import com.reza.permohonancuti.model.Users;
import com.reza.permohonancuti.repository.BucketApprovalRepository;
import com.reza.permohonancuti.repository.PositionLeaveRepository;
import com.reza.permohonancuti.repository.UserLeaveRequestRepository;
import com.reza.permohonancuti.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class RequestLeaveController {

	@Autowired
	private UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	private PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private BucketApprovalRepository bucketApprovalRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entitiy To DTO
	public UserLeaveRequestDTO convertULRToDTO (UserLeaveRequest userLeaveRequest) {
		UserLeaveRequestDTO uLRDto = modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
		
		if (userLeaveRequest.getUsers() != null) {
			UsersDTO uDto = modelMapper.map(userLeaveRequest.getUsers(), UsersDTO.class);
			uLRDto.setUsersDto(uDto);
		}
	return uLRDto;
	}

//	Convert DTO to Entity
	public UserLeaveRequest convertULRToEntity (UserLeaveRequestDTO userLeaveRequestDTO) {
		UserLeaveRequest uLR = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
		
		if (userLeaveRequestDTO.getUsersDto() != null) {
			Users usr = modelMapper.map(userLeaveRequestDTO.getUsersDto(), Users.class);
			uLR.setUsers(usr);
		}
	return uLR;
	}

	
	@GetMapping("/requestleave/tes/{id}")
	public int determineSisaCuti (@PathVariable(value = "id") Integer idUser) {
		
		int total=0;
		Users usr = usersRepository.findById(idUser).orElse(null);
		
		for (PositionLeave pl :positionLeaveRepository.findAll()) {
			
			if (pl.getPositions().getIdPosition() == usr.getPositions().getIdPosition()) {				
				total = pl.getTotalLeave();
			}
		}
		
		for (UserLeaveRequest ulr : userLeaveRequestRepository.findAll()) {
			
			Date dateFrom = ulr.getLeaveDateFrom();
			LocalDate fromDate = dateFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			
			Date dateTo = ulr.getLeaveDateTo();
			LocalDate toDate = dateTo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
						
			if (ulr.getUsers().getIdUser() == usr.getIdUser()) {
			
//				for (BucketApproval ba : bucketApprovalRepository.findAll()) {	
//					if (ba.getStatus().equalsIgnoreCase("approved")) {
//						Long days = ChronoUnit.DAYS.between(fromDate, toDate);
//						total -= days.intValue();
//					}
//				}
			}
		}
		
	return total;
	}
	
	
	@PostMapping("/requestleave/add")
	public HashMap<String, Object> addUserLeaveRequest(@Valid @RequestBody UserLeaveRequestDTO userLeaveRequestDTO) {
		
//		Covert date to LocalDate
		Date dateFrom = userLeaveRequestDTO.getLeaveDateFrom();
		LocalDate fromDate = dateFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();	
		
		Date dateTo = userLeaveRequestDTO.getLeaveDateTo();
		LocalDate toDate = dateTo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

//		Menghitung jumlah hari total cuti
		Long days = ChronoUnit.DAYS.between(fromDate, toDate);
		
		userLeaveRequestDTO.setSisaCuti(determineSisaCuti(userLeaveRequestDTO.getUsersDto().getIdUser()));
		
		int cuti = determineSisaCuti(userLeaveRequestDTO.getUsersDto().getIdUser());
		String message="";		
		
		HashMap<String, Object> hasMapUserLeaveReq = new HashMap<String, Object>();		
		
		UserLeaveRequest uLR = convertULRToEntity(userLeaveRequestDTO);
		
			for (PositionLeave pl : positionLeaveRepository.findAll()) {
				for (Users usr : usersRepository.findAll()) {					
				
					if (usr.getIdUser() == userLeaveRequestDTO.getUsersDto().getIdUser()) {
						
			//			Kondisi apabila total cuti sudah habis
						 if (cuti == 0 ) {
							message = "Maaf permohonan jatah cuti anda telah habis.";
						}
			//			Kondisi apabila pengajuan cuti lebih besar dari pada total cuti
						else if (days > cuti) {
							message = "Mohon maaf, jatah cuti anda tidak cukup untuk digunakan pada tanggal " + fromDate + " sampai " + toDate + " ("  + days + " hari). Jatah cuti anda yang tersisa hanya " + pl.getTotalLeave();
						}
			//			Kondisi apabila tanggal pengajuan cuti lebih besar dari pada tanggal cuti
						else if (fromDate.compareTo(toDate) == 1) {
							message ="Maaf, tanggal yang anda ajukan tidak valid";
						}
			//			Kondisi apabila tanggal pengajuan cuti lebih kecil dari pada sekarang
						else if (fromDate.compareTo(LocalDate.now()) < 0) {
							message = "Maaf, Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.";
						}
						else {
			//			Kondisi apabila valid
							message = "Permohonan anda sedang di proses.";						
						}
					}	
				}
			}	
			
//			Untuk set id bucket approval dan set status di table bucket approval
			if (message.equalsIgnoreCase("Permohonan anda sedang di proses.")) {
			BucketApproval ba = new BucketApproval();
			ba = bucketApprovalRepository.save(ba);
			ba.setStatus("waiting");
			uLR.setBucketApproval(ba);
			hasMapUserLeaveReq.put("Data" , userLeaveRequestRepository.save(uLR));
			}
		
			
		hasMapUserLeaveReq.put("Message" , message);
	return hasMapUserLeaveReq;		
	}
	
	
//	method untuk lihat data (method sementara)
	@GetMapping("/requestleave/all")
	public HashMap<String, Object> getAllUserLeaveRequest() {
		
		HashMap<String, Object> hasMapUserLeaveReq = new HashMap<String, Object>();
		ArrayList<UserLeaveRequestDTO> uLRDtoList = new ArrayList<UserLeaveRequestDTO>();
		
		for (UserLeaveRequest uLR : userLeaveRequestRepository.findAll()) {
			UserLeaveRequestDTO uLRDto = convertULRToDTO(uLR);
			uLRDtoList.add(uLRDto);
		}
		hasMapUserLeaveReq.put("Message" , "Show All Data");
		hasMapUserLeaveReq.put("Total", uLRDtoList.size());
		hasMapUserLeaveReq.put("Data", uLRDtoList);
			
	return hasMapUserLeaveReq;
	}

	@GetMapping("/requestleave/")
	@ResponseBody
	public List<UserLeaveRequest> getListRequestLeave(@RequestParam (defaultValue = "0") Integer userId, @RequestParam (defaultValue = "3" ) Integer total, @RequestParam(defaultValue = "description") String descriptionSortBy) {
			
		Pageable paging = PageRequest.of(userId, total, Sort.by(descriptionSortBy).ascending());
		
		Page<UserLeaveRequest> pagedResult = userLeaveRequestRepository.findAll(paging);
		
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		}
		else {
			return new ArrayList<UserLeaveRequest>();
		}
	}
	
	
//	@GetMapping("/requestleave/")
//	@ResponseBody
//	public List<UserLeaveRequest> getListRequestLeave(@RequestParam (defaultValue = "id") Integer userId, @RequestParam (defaultValue = "3" ) Integer total, @RequestBody (required = false) BucketApproval ulr) {
//			
//		Pageable paging = PageRequest.of(userId, total);
//		
//		Page<UserLeaveRequest> pagedResult = userLeaveRequestRepository.findAll(paging);
//		
//		if (pagedResult.hasContent()) {
//			return pagedResult.getContent();
//		}
//		else {
//			return new ArrayList<UserLeaveRequest>();
//		}
//
//	}
	
}
