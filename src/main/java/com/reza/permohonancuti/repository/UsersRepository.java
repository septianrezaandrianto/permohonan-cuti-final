package com.reza.permohonancuti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reza.permohonancuti.model.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer>{

	
}
