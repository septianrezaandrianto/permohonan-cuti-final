package com.reza.permohonancuti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.reza.permohonancuti.model.UserLeaveRequest;

@Repository
public interface UserLeaveRequestRepository extends PagingAndSortingRepository<UserLeaveRequest, Integer> {

	
}
