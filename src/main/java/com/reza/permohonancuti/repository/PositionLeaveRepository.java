package com.reza.permohonancuti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reza.permohonancuti.model.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Integer>{

	
}
